import 'package:flutter/material.dart';
import 'package:tci_flutter_bootcamp/pages/home_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final usernameCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();
  final errors = <String, String>{};
  String uname = '';
  @override
  Widget build(BuildContext context) {
    print('BUILD');
    return Scaffold(
      backgroundColor: const Color(0xff256EFF),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Container(
              padding: const EdgeInsets.all(24),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(14),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.network(
                    'https://cdn.discordapp.com/attachments/1115206343985475634/1148464936905887844/icon2.png',
                    height: 24,
                  ),
                  const SizedBox(height: 12),
                  MyTextField(
                    controller: usernameCtrl,
                    label: "Username",
                    hint: "Input Your Username",
                    prefix: const Icon(
                      Icons.person,
                      color: Colors.black,
                    ),
                    errorText: errors['username'],
                  ),
                  const SizedBox(height: 12),
                  MyTextField(
                    controller: passwordCtrl,
                    label: "Password",
                    hint: "Input Your Password",
                    obscure: true,
                    prefix: const Icon(
                      Icons.lock,
                      color: Colors.black,
                    ),
                    errorText: errors['password'],
                  ),
                  const SizedBox(height: 12),
                  SizedBox(
                    width: double.infinity,
                    height: 40,
                    child: ElevatedButton(
                      onPressed: () {
                        final String username  = usernameCtrl.text;
                        final String password  = passwordCtrl.text;
                        errors.clear();
                        if(username.isEmpty){
                          setState(() {
                            errors['username'] = 'username ga boleh kosong';
                          });
                        }
                        if(password.isEmpty){
                          setState(() {
                            errors['password'] = 'password ga boleh kosong';
                          });
                        }
                        if(errors.isEmpty){
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => const HomePage(),
                            ),
                          );
                        }
                        /**/
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xff256EFF),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                        )
                      ),
                      child: const Text(
                        'Login',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyTextField extends StatelessWidget {
  const MyTextField({
    super.key,
    required this.label,
    required this.hint,
    this.prefix,
    this.obscure = false,
    this.errorText, this.onChanged, this.controller, this.onSubmitted,
  });

  final String label, hint;
  final Widget? prefix;
  final bool obscure;
  final String? errorText;
  final Function(String)? onChanged;
  final Function(String)? onSubmitted;
  final TextEditingController? controller;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: const TextStyle(
            fontSize: 14,
            color: Color(0xff101117),
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(height: 12),
        TextField(
          controller: controller,
          onChanged: onChanged,
          onSubmitted: onSubmitted,
          obscureText: obscure,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: const BorderSide(
                color: Color(0xffE7EAEF),
                width: 1,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: const BorderSide(
                color: Colors.blue,
                width: 2,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
              borderSide: const BorderSide(
                color: Colors.red,
              ),
            ),
            hintText: hint,
            hintStyle: const TextStyle(
              fontSize: 14,
              color: Color(0xffBEBFC4),
              fontWeight: FontWeight.w600,
            ),
            prefixIcon: prefix,
            errorText: errorText,
          ),
        ),
      ],
    );
  }
}
