import 'package:flutter/material.dart';
import 'package:tci_flutter_bootcamp/pages/login_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Todo> todos = [
    Todo("task1", false),
    Todo("task2", false),
    Todo("task3", false),
    Todo("task4", false),
  ];
  final todoCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TODO APP"),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: MyTextField(
              label: "label",
              hint: " hint",
              controller: todoCtrl,
              onSubmitted: (value) {
                setState(() {
                  todos.add(Todo(value, false));
                });
                todoCtrl.clear();
              },
            ),
          ),
          const Divider(),
          FilledButton(onPressed: (){
            setState(() {
              for(final todo in todos){
                todo.status = true;
              }
            });
          }, child: const Text("check all")),
          FilledButton(onPressed: (){
            setState(() {
              for(final todo in todos){
                todo.status = false;
              }
            });
          }, child: const Text("uncheck all")),
          const Divider(),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, index) {
                final todo = todos[index];
                return InkWell(
                  onTap: () {
                    setState(() {
                      todo.status = !todo.status;
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                    child: Row(
                      children: [
                        Icon(todo.status ?Icons.check_box:Icons.check_box_outline_blank),
                        const SizedBox(width: 8),
                        Text(
                          todo.task,
                          style: const TextStyle(
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: todos.length,
            ),
          ),
        ],
      ),
    );
  }
}

class Todo {
  String task;
  bool status;

  Todo(this.task, this.status);
}
